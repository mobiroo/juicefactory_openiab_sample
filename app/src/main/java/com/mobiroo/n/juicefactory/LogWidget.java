package com.mobiroo.n.juicefactory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobiroo.xgen.core.logger.Logger;

public class LogWidget extends LinearLayout {
    private static final String TAG = LogWidget.class.getSimpleName();

    public LogWidget(Context context) {
        super(context);
        init();
    }

    public LogWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LogWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LogWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.log_widget, this);
    }

    private TextView tvLogMessage;
    private Button btnClearStatus;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvLogMessage = (TextView) this
                .findViewById(R.id.tvMessage);

        btnClearStatus = (Button) findViewById(R.id.btnClearStatus);
        btnClearStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvLogMessage.setText("");
            }
        });
    }

    public void addMessage(String message) {
        Logger.debug(TAG + "::addMessage: message=" + message);
        String currentMessage = (String) tvLogMessage.getText();
        String newMessage = message + "\n" + currentMessage;
        tvLogMessage.setText(newMessage);
    }
}
