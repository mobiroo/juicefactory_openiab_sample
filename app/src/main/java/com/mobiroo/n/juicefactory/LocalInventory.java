package com.mobiroo.n.juicefactory;

import android.content.Context;
import android.content.SharedPreferences;

import com.mobiroo.xgen.core.logger.Logger;

public class LocalInventory {
    private static final String TAG = "Juice - " + LocalInventory.class.getSimpleName();

    private static String PREF_LOCAL_INVENTORY = "localInventory";
    private final OnInventoryChangeListener inventoryChangeListener;
    private SharedPreferences sharedPreferences;

    LocalInventory(Context context, OnInventoryChangeListener inventoryChangeListener) {
        this.sharedPreferences = context.getSharedPreferences(PREF_LOCAL_INVENTORY, Context.MODE_PRIVATE);
        this.inventoryChangeListener = inventoryChangeListener;
    }

    void incrementQty(String sku) {
        int oldCount = this.sharedPreferences.getInt(sku, 0);
        int newCount = oldCount + 1;
        Logger.info(TAG + "::incrementQty  oldCount=" + oldCount + ", newCount= " + newCount);
        sharedPreferences.edit().putInt(sku, newCount).commit();
        if (inventoryChangeListener != null) inventoryChangeListener.onChange(sku, newCount);
    }

    String getQtyAsText(String sku) {
        int val = sharedPreferences.getInt(sku, 0);
        String valString = String.valueOf(val);
        Logger.info(TAG + "::getQtyAsText: qty for " + sku + " = " + valString);
        return valString;
    }

    interface OnInventoryChangeListener {
        void onChange(String sku, int newCount);
    }
}
