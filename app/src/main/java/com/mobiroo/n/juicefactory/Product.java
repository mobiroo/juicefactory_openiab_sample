package com.mobiroo.n.juicefactory;

import android.support.annotation.DrawableRes;

class Product {
    int requestCode;
    int imageResource;
    String sku;
    String name;
    String desc;
    String type; //consumable or entitlement


    public Product(String name, String sku, String desc, String type, @DrawableRes int imageResource, int iapRequestCode) {
        this.name = name;
        this.sku = sku;
        this.desc = desc;
        this.type = type;
        this.requestCode = iapRequestCode;
        this.imageResource = imageResource;
    }

}