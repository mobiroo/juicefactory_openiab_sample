package com.mobiroo.n.juicefactory;

import android.app.Application;

import com.mobiroo.n.juicefactory.iap.InAppConfig;

public class MyApplication extends Application {
    public MyApplication() {
        InAppConfig.init();
    }
}
