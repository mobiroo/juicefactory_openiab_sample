package com.mobiroo.n.juicefactory.iap;

import org.onepf.oms.SkuManager;

import java.util.HashMap;
import java.util.Map;

public final class InAppConfig
{

	public static final String NAME_MOBIROO = "com.mobiroo.xgen";
	
	
	final public static String MOBIROO_PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkd5P6tcVYNa4xFk"
			+ "KHt592HlOYrFq9T1OlDvkFpkdN9o+0cvTF2iQ8fjPQJ7eG6cr7Hc7Ch0mRtr1g4LFLQI92OrLAGTCag5Uyk/z4u"
			+ "KZUTtN/o54PSWMx16XiuzaK66MWmNfTnEVTb4uYXqnajZkNjfA6QdxT03c3zMLUwuf0kKwSaRjmWyY7Rb+mjRmx"
			+ "f09FIktB9fZPg1LSXXISalW4SC5hH7CYPxca1NuJmLq1uN0ANRgchh6jVXOhb+IAp9BugjlJ0LAFzQU76mOHoon"
			+ "yy/kD1abGo/WruKh8+LuCrjvV0p7EJxR0AZEFnGystjVLwLsMPhHs7q7Tp77NplhVwIDAQAB";
	
	

	public static Map<String, String> STORE_KEYS_MAP;

	public static final String SKU_APPLE = "sku_apple";

	public static final String SKU_BLENDER = "sku_blender";

	public static void init()
	{

		STORE_KEYS_MAP = new HashMap<String, String>();
		STORE_KEYS_MAP.put(NAME_MOBIROO, MOBIROO_PUB_KEY);
		
		SkuManager.getInstance().mapSku(SKU_APPLE, "org.onepf.store", "iap_mob_apple")
								.mapSku(SKU_BLENDER, "org.onepf.store", "iap_mob_blender");
		
		
		SkuManager.getInstance().mapSku(SKU_APPLE, NAME_MOBIROO, "iap_mob_apple")
								.mapSku(SKU_BLENDER, NAME_MOBIROO, "iap_mob_blender");

	}

	private InAppConfig()
	{
	}
}