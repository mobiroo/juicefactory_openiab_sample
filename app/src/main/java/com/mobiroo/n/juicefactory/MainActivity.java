package com.mobiroo.n.juicefactory;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mobiroo.n.juicefactory.iap.InAppConfig;
import com.mobiroo.xgen.core.helper.GsonHelper;
import com.mobiroo.xgen.core.logger.Logger;

import org.onepf.oms.OpenIabHelper;
import org.onepf.oms.appstore.googleUtils.IabHelper;
import org.onepf.oms.appstore.googleUtils.IabResult;
import org.onepf.oms.appstore.googleUtils.Inventory;
import org.onepf.oms.appstore.googleUtils.Purchase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.mobiroo.n.juicefactory.R.id.btnBuy;

public class MainActivity extends Activity implements LocalInventory.OnInventoryChangeListener {

    private static final String TAG = "Juice - " + MainActivity.class.getSimpleName();

    private ProgressDialog progressDialog;
    private View errorContainer;
    private View contentContainer;
    private OpenIabHelper openIabHelper;

    private Button btnBuyApple;
    private TextView tvQtyApple;

    private TextView tvQtyBlender;
    private Button btnBuyBlender;

    private HashMap<String, Product> skuProductMap = new HashMap<String, Product>();
    private LocalInventory localInventory;
    private LogWidget logWidget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.debug(TAG + "::onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logWidget = (LogWidget) findViewById(R.id.log_widget);
        contentContainer = findViewById(R.id.content_container);
        errorContainer = findViewById(R.id.error_container);
        initProducts();
        localInventory = new LocalInventory(this, this);
        initViews();
        initOpenIabHelper();
    }

    private void initProducts() {
        Logger.debug(TAG + ":initProducts");
        final Product appleProduct = new Product("Apple", InAppConfig.SKU_APPLE, "consumable red delicious apple", "consumable", R.drawable.apple, 166);
        final Product blenderProduct = new Product("Blender", InAppConfig.SKU_BLENDER, "entitlement blender machine", "entitlement", R.drawable.blender, 167);
        skuProductMap.put(InAppConfig.SKU_APPLE, appleProduct);
        skuProductMap.put(InAppConfig.SKU_BLENDER, blenderProduct);
    }


    private void initViews() {
        Logger.debug(TAG + ":initViews");
        final Product appleProduct = skuProductMap.get(InAppConfig.SKU_APPLE);
        View appleContainer = contentContainer.findViewById(R.id.rlApple);
        TextView tvTitleApple = (TextView) appleContainer.findViewById(R.id.tvProductTitle);
        tvTitleApple.setText(appleProduct.name);
        tvTitleApple.setCompoundDrawablesWithIntrinsicBounds(0, appleProduct.imageResource, 0, 0);
        tvQtyApple = (TextView) appleContainer.findViewById(R.id.tvQtyAvailable);
        tvQtyApple.setText(localInventory.getQtyAsText(appleProduct.sku));
        btnBuyApple = (Button) appleContainer.findViewById(btnBuy);
        btnBuyApple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(appleProduct);
            }
        });


        final Product blenderProduct = skuProductMap.get(InAppConfig.SKU_BLENDER);
        View blenderContainer = contentContainer.findViewById(R.id.rlBlender);
        TextView tvTitleBlender = (TextView) blenderContainer.findViewById(R.id.tvProductTitle);
        tvTitleBlender.setText(blenderProduct.name);
        tvTitleBlender.setCompoundDrawablesWithIntrinsicBounds(0, blenderProduct.imageResource, 0, 0);
        tvQtyBlender = (TextView) blenderContainer.findViewById(R.id.tvQtyAvailable);
        tvQtyBlender.setText(localInventory.getQtyAsText(blenderProduct.sku));
        btnBuyBlender = (Button) blenderContainer.findViewById(btnBuy);
        btnBuyBlender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy(blenderProduct);
            }
        });
    }

    private void showErrorOnly(String message) {
        Logger.error(TAG + ":showErrorOnly:message=" + message);
        contentContainer.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        TextView tv = (TextView) errorContainer.findViewById(R.id.tvError);
        tv.setText(message);
        tv.setError(message);
    }

    private void showContentOnly() {
        Logger.debug(TAG + ":showContentOnly");
        contentContainer.setVisibility(View.VISIBLE);
        errorContainer.setVisibility(View.GONE);
    }


    private void initOpenIabHelper() {
        Logger.debug(TAG + "::initOpenIabHelper");
        showProgress("initOpenIabHelper initializing openiab");
        //@formatter:off
        OpenIabHelper.Options options = new OpenIabHelper.Options.Builder()
                .addAvailableStoreNames(InAppConfig.NAME_MOBIROO)
                .addPreferredStoreName(InAppConfig.NAME_MOBIROO)
                .addStoreKeys(InAppConfig.STORE_KEYS_MAP)
                .setVerifyMode(OpenIabHelper.Options.VERIFY_ONLY_KNOWN)
                .setStoreSearchStrategy(OpenIabHelper.Options.SEARCH_STRATEGY_INSTALLER_THEN_BEST_FIT)
                .build();

        //@formatter:on
        openIabHelper = new OpenIabHelper(this, options);

        openIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                Logger.debug(TAG + "::initOpenIabHelper ::onIabSetupFinished");
                if (result.isFailure()) {
                    Logger.error(TAG + ":initOpenIabHelper onIabSetupFinished resulted failure :( result=" + result);
                    showErrorOnly(result.getMessage());
                } else {
                    Logger.debug(TAG + ":initOpenIabHelper onIabSetupFinished resulted success :) result=" + result);
                    checkInventory();
                    showContentOnly();
                }
                logWidget.addMessage(result.getMessage());
                hideProgress();
            }
        });

    }


    private boolean verifyDeveloperPayload(Purchase purchase) {
        Logger.debug(TAG + "::verifyDeveloperPayload: purchase" + purchase);
        return true; // for now
    }

    private void checkInventory() {
        Logger.debug(TAG + "::checkInventory");
        openIabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {

            @Override
            public void onQueryInventoryFinished(final IabResult result, Inventory inv) {
                Logger.debug(TAG + "::checkInventory ::onQueryInventoryFinished");
                if (result.isSuccess()) {
                    Logger.debug(TAG + "::checkInventory() :purchaseManager:getPurchases:inventory= " + GsonHelper.toGsonString(inv));
                    List<Purchase> purchases = inv.getAllPurchases();
                    List<String> purchaseSkus = getPurchaseSkus(purchases);
                    Logger.debug(TAG + "::checkInventory :onQueryInventoryFinished: purchases size: " + purchases.size() + ", purchased skus = " + Arrays.toString(purchaseSkus.toArray()) + ", purchases=" + GsonHelper.toGsonString(purchases));
                    List<Purchase> consumablesPurchases = filterConsumablesPurchases(purchases);
                    Logger.debug(TAG + "::checkInventory :onQueryInventoryFinished: consumablesPurchases size: " + consumablesPurchases.size());
                    if (consumablesPurchases.size() > 0) {
                        openIabHelper.consumeAsync(consumablesPurchases, new IabHelper.OnConsumeMultiFinishedListener() {
                            @Override
                            public void onConsumeMultiFinished(List<Purchase> purchases, List<IabResult> results) {
                                for (int i = 0; i < purchases.size(); i++) {
                                    Purchase p = purchases.get(i);
                                    IabResult iabResult = results.get(i);
                                    Logger.debug(TAG + ":checkInventory onQueryInventoryFinished onConsumeMultiFinished iabResult=" + iabResult);
                                    if (iabResult.isSuccess()) {
                                        localInventory.incrementQty(p.getSku());
                                    }
                                }
                            }

                        });
                    }
                } else {
                    Logger.warn(TAG + ":checkInventory() ::onQueryInventoryFinished result:isFailure " + result);
                }
                logWidget.addMessage(result.getMessage());
            }

            private List<String> getPurchaseSkus(List<Purchase> purchases) {
                Logger.debug(TAG + "::checkInventory ::getPurchaseSkus");
                List<String> skus = new ArrayList<>();
                for (Purchase p : purchases) {
                    skus.add(p.getSku());
                }
                return skus;
            }

            private List<Purchase> filterConsumablesPurchases(List<Purchase> purchases) {
                Logger.debug(TAG + "::checkInventory ::filterConsumablesPurchases");
                List<Purchase> filteredProducts = new ArrayList<>();
                for (Purchase p : purchases) {
                    String sku = p.getSku();
                    Product product = skuProductMap.get(sku);
                    if (product != null && product.type.equals("consumable")) {
                        filteredProducts.add(p);
                    }
                }
                return filteredProducts;
            }
        });
    }

    private void updateQuantities() {
        Logger.debug(TAG + ":updateQuantities");
        String qtyApple = localInventory.getQtyAsText(InAppConfig.SKU_APPLE);
        tvQtyApple.setText(qtyApple);

        String qtyBlender = localInventory.getQtyAsText(InAppConfig.SKU_BLENDER);
        tvQtyBlender.setText(qtyBlender);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.debug(TAG + "::onActivityResult: resultCode=" + requestCode + ", result=" + resultCode + ", data=", data.getExtras());
        if (!openIabHelper.handleActivityResult(requestCode, resultCode, data)) {
            Logger.warn(TAG + "::onActivityResult: openIabHelper could not handle the activity result");
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void hideProgress() {
        Logger.debug(TAG + "::hideProgress");
        setProgress(false, "");
    }

    private void showProgress(String message) {
        Logger.debug(TAG + "::showProgress");
        setProgress(true, message);
    }

    /***
     * @param enable
     * @param message optional pass null if disabling
     */
    private void setProgress(boolean enable, String message) {
        // dismiss if false
        Logger.debug(TAG + "::setProgress");
        if (!enable) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } else {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(MainActivity.this);
            }
            if (!progressDialog.isShowing()) {
                progressDialog.setMessage(message);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
    }


    private void buy(final Product product) {
        Logger.info(TAG + "::buy: sku=" + product.sku);
        openIabHelper.launchPurchaseFlow(this, product.sku, product.requestCode, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                Logger.debug(TAG + ":buy :launchPurchaseFlow: :onIabPurchaseFinished result=" + result + ", purchase=" + GsonHelper.toGsonString(purchase));
                if (!result.isSuccess()) {
                    Logger.warn(TAG + ":: buy result was not success");
                    logWidget.addMessage(result.getMessage());
                    return;
                }
                if (validateSignature(purchase)) {
                    //try to consume immediately if consumable
                    openIabHelper.consumeAsync(purchase, new IabHelper.OnConsumeFinishedListener() {
                        @Override
                        public void onConsumeFinished(Purchase purchase, IabResult result) {
                            Logger.debug(TAG + "::onConsumeFinished: " + result);
                            if (result.isSuccess()) {
                                localInventory.incrementQty(purchase.getSku());
                            }
                        }
                    });
                } else {
                    Logger.warn(TAG + ":: buy Purchase/Signature is NOT valid");
                    logWidget.addMessage("Signature is NOT valid");
                    return;
                }
            }
        });
    }

    private boolean validateSignature(Purchase purchase) {
        String origJson = purchase.getOriginalJson();//original purchase JSON data as received from the server.
        String signature = purchase.getSignature();//the signed json data, data will be signed by the server using mobiroo private key.
        boolean valid = org.onepf.oms.appstore.googleUtils.Security.verifyPurchase(InAppConfig.MOBIROO_PUB_KEY, origJson, signature);
        Logger.debug(TAG + "::validateSignature isValid=" + valid);
        return valid;
    }

    @Override
    public void onChange(String sku, int newCount) {
        Logger.debug(TAG + "::onChange");
        updateQuantities();
    }
}
