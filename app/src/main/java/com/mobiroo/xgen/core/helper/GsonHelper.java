package com.mobiroo.xgen.core.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

public class GsonHelper {
    public static String toGsonString(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }

    public static <T> T getJSONObject(String jsonString, Class<T> classOfT) throws JsonSyntaxException {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, classOfT);
    }

    public static <T> T getJSONObject(String jsonString, Type typeOfT) throws JsonSyntaxException {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, typeOfT);
    }
}
