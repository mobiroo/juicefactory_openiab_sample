package com.mobiroo.xgen.core.logger;

import android.os.Bundle;
import android.util.Log;

import java.util.Set;

public class Logger {


    public enum DebugMode {
        ON, OFF
    }

    private static DebugMode mode = DebugMode.ON;

    private static final String LOG_TAG = "Juice";


    final public static void setDebugMode(DebugMode debugMode) {
        mode = debugMode;
    }

    final public static DebugMode getDebugMode() {
        return mode;
    }

    final public static void debug(String msg) {
        debug(LOG_TAG, msg);
    }

    final public static void debug(String tag, String msg) {
        if (mode == DebugMode.ON)
            Log.d(tag, msg);
    }

    final public static void error(String msg) {
        error(LOG_TAG, msg);
    }

    final public static void error(String tag, String msg) {
        if (mode == DebugMode.ON)
            Log.e(tag, msg);
    }

    final public static void verbose(String msg) {
        verbose(LOG_TAG, msg);
    }

    final public static void verbose(String tag, String msg) {
        if (mode == DebugMode.ON)
            Log.v(tag, msg);
    }

    final public static void info(String msg) {
        info(LOG_TAG, msg);
    }

    final public static void info(String tag, String msg) {
        if (mode == DebugMode.ON)
            Log.i(tag, msg);
    }

    final public static void warn(String msg) {
        warn(LOG_TAG, msg);
    }

    final public static void warn(String tag, String msg) {
        if (mode == DebugMode.ON)
            Log.w(tag, msg);
    }

    final public static void printStackTrace(Exception ex) {
        printStackTrace(LOG_TAG, ex);
    }

    final public static void printStackTrace(String tag, Exception ex) {
        if (mode == DebugMode.ON && ex != null) {
            Log.e(tag, "=========== Exception ==========");
            ex.printStackTrace();
        }
    }

    final public static void error(String msg, Throwable tr) {
        error(LOG_TAG, msg, tr);
    }

    final public static void error(String tag, String msg, Throwable tr) {
        if (mode == DebugMode.ON)
            Log.e(tag, msg, tr);
    }

    final public static void debug(String tag, Bundle bundle) {
        StringBuilder sb = new StringBuilder(tag).append("::bundle=[");
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                sb.append(key).append("=").append(bundle.get(key)).append(", ");
            }
        } else {
            sb.append("bundle is null");
        }
        sb.append("]");
        debug(LOG_TAG, sb.toString());
    }

}
